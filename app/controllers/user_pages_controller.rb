class UserPagesController < ApplicationController
  def regist
    @user = User.new
  end

  def profile
    @user = User.find_by(id: session[:user_id])
  end

  def edit
  end

  def login
  end

  def favdeleteselect
    @user = User.find(session[:user_id])
  end

  def favdelete
    @user = User.find(session[:user_id])
    favid = params[:favid]
    @favrecipe = Dish.find(favid)
    @user.dishes.destroy(@favrecipe)
    redirect_to :back
  end

  def destroy
    @user = User.find_by(id: session[:user_id])
    @user.destroy
  end

  def allergies
    @all = Allergy.all
    @user = User.find_by(id: session[:user_id])
  end

  def allergiesinput
#    params[:alle].inspect
#    len = params[:alle].length
#    @allstr = ""
#    i = 0
#    len.each do |all|
#      flg = false
#      if params[:alle][i].nil?
#      else
#        flg = true
#        @allstr.concat(params[:alle][i].to_s)
#        if i != len - 1 && flg
#            @allstr.concat(",")
#        end
#      end
#      i += 1
#    end
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if params[:back]
      render 'regist'
    elsif @user.save
      @user.send_activation_email
      flash[:info] = "入力されたメールアドレスにメールを送信しました。"
      redirect_to root_url
    else
      render 'regist'
    end
  end

  def confirm
    @user = User.new(user_params)
    render 'regist' if @user.invalid?
  end

  private

  def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
  end
end
